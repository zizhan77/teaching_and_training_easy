import Vue from 'vue'
import {
    LayoutHeader,
    LayoutFooter,
    LayoutLinks,
    LayoutAdvisory
} from '~/components'

Vue.use(LayoutHeader)
Vue.use(LayoutFooter)
Vue.use(LayoutLinks)
Vue.use(LayoutAdvisory)