export default {
    titleTemplate: '%s - 教培易',
    meta: [
        { charset: 'utf-8' },
        { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' },
        { name: 'applicable-device', content: 'pc, mobile' },
        { name: 'renderer', content: 'webkit' },
        { 'http-equiv': 'X-UA-Compatible', content: 'IE=edge' },
        { 'http-equiv': 'Cache-Control', content: 'no-transform' },
        { 'http-equiv': 'Cache-Control', content: 'no-siteapp' }
    ]
}
