import app from './app'
import head from './head'
import plugins from './plugins'
import css from './css'
import router from './router'

export default {
    app,
    head,
    plugins,
    css,
    router
}
