import Links from './src/links'

Links.install = function (Vue) {
    Vue.component(Links.name, Links)
}

export default Links