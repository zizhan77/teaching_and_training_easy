import LayoutHeader from './header'
import LayoutFooter from './footer'
import LayoutLinks from './links'
import LayoutAdvisory from './advisory'

export {
    LayoutHeader,
    LayoutFooter,
    LayoutLinks,
    LayoutAdvisory
}