import Advisory from './src/advisory'

Advisory.install = function (Vue) {
    Vue.component(Advisory.name, Advisory)
}

export default Advisory