import config from './src/config'

export default {
    srcDir: 'src/',
    loading: config.app.loading,
    build: {
        publicPath: '/dist/',
        html: {
            minify: {
                collapseBooleanAttributes: false,
                collapseWhitespace: false,
                decodeEntities: false,
                minifyCSS: false,
                minifyJS: false,
                processConditionalComments: false,
                removeAttributeQuotes: false,
                removeComments: false,
                removeEmptyAttributes: false,
                removeOptionalTags: false,
                removeRedundantAttributes: false,
                removeScriptTypeAttributes: false,
                removeStyleLinkTypeAttributes: false,
                removeTagWhitespace: false,
                sortClassName: false,
                trimCustomFragments: false,
                useShortDoctype: false
            }
        },
        extractCSS: true,
        filenames: {
            app: ({ isDev }) => isDev ? '[name].js' : '[name].[chunkhash].js',
            chunk: ({ isDev }) => isDev ? '[name].js' : '[name].[chunkhash].js',
            css: ({ isDev }) => isDev ? '[name].js' : '[name].[contenthash].css',
            img: ({ isDev }) => isDev ? '[path][name].[ext]' : 'images/[name].[hash:7].[ext]',
            font: ({ isDev }) => isDev ? '[path][name].[ext]' : 'fonts/[name].[hash:7].[ext]',
            video: ({ isDev }) => isDev ? '[path][name].[ext]' : 'videos/[name].[hash:7].[ext]'
        }
    },
    generate: {
        dir: 'public',
        fallback: false,
        subFolders: false
    },
    head: config.head,
    plugins: config.plugins,
    css: config.css,
    router: config.router
}
